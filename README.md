# Low Pressure Natural Gas Net Numeric Calculation

The purpose of this project is a low pressure natural gas network design and calculation.
In particular, appropriate diameters are selected for the pipes of the network
according to an iterative numerical procedure(Newton-Ralphson). The above process leads
to an optimal economic solution, taking into account the specifications (the drop of
static pressure may not exceed a limit, as well as the flow speed in the pipelines).

Also, an analysis of the change in total manometric pressure is carried out at the network nodes during the day. The aim of the analysis below is to create a model which takes parametric data for any network and finds the static and total pressure at each of its nodes.