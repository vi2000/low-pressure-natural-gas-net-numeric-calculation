function dF = dF_parametric(NNODE,indxneib_element,indxneib_node,p0,k_linear_tel)
dF = zeros(NNODE);
dF(1) = 1;
dF_neg_sum = 0;
for i=2:NNODE
    
    for k = 1:5
      if (indxneib_node(i,k)~=0)
     dF(i,indxneib_node(i,k)) =  0.5/sqrt(k_linear_tel(indxneib_element(i,k)))/(abs(p0(i)-p0(indxneib_node(i,k))))^0.5 ;
     dF_neg_sum = dF_neg_sum + dF(i,indxneib_node(i,k));
      end
    end
dF(i,i) = - dF_neg_sum;
dF_neg_sum = 0;
end
end

