function F = F_parametric(NNODE,QINOUT,indxneib_element,indxneib_node,p0,k_tel)
F = zeros(NNODE,1);
Fsum = zeros(NNODE,1);
F(1) = 0;

for i=2:NNODE
    for k = 1:5
      if (indxneib_node(i,k)~= 0)
        if (p0(i) <= p0(indxneib_node(i,k)))
          sign = +1;
        else
          sign = -1;
        end 
     Fsum(i) = Fsum(i)+ sign*(abs((p0(i)-p0(indxneib_node(i,k)))/k_tel(indxneib_element(i,k))))^0.5;
      end
    end
     F(i) = QINOUT(i) + Fsum(i);
end
end