function [p,k_tel] = NR_parametric(Q,Qin,d)
%% Γραμμικές Απώλειες
rho_fa = 0.79;
lamda = get_lamda(Q,d);
k12 = k_linear(d(1),lamda(1),5);
k23 = k_linear(d(2),lamda(2),3.5);
k34 = k_linear(d(3),lamda(3),16);
k45 = k_linear(d(4),lamda(4),5);
k56 = k_linear(d(5),lamda(5),5);
k67 = k_linear(d(6),lamda(6),16);
k78 = k_linear(d(7),lamda(7),16);
k89 = k_linear(d(8),lamda(8),5);
k910 = k_linear(d(9),lamda(9),5);
k310 = k_linear(d(10),lamda(10),16);
k311 = k_linear(d(11),lamda(11),5);
k711 = k_linear(d(12),lamda(12),5);
k312 = k_linear(d(13),lamda(13),3);
k1213 = k_linear(d(14),lamda(14),16);
k1314 = k_linear(d(15),lamda(15),5);
k1415 = k_linear(d(16),lamda(16),5);
k1516 = k_linear(d(17),lamda(17),16);
k1617 = k_linear(d(18),lamda(18),16);
k1718 = k_linear(d(19),lamda(19),5);
k1819 = k_linear(d(20),lamda(20),5);
k1219 = k_linear(d(21),lamda(21),16);
k1220 = k_linear(d(22),lamda(22),5);
k1620 = k_linear(d(23),lamda(23),5);

%% Εντοπισμένες απώλειες 
k12 = k12 +0.5*8*rho_fa/(pi^2*d(1)^4);
k23 = k23 + 2*8*rho_fa/(pi^2*d(2)^4)+0.5*8*rho_fa/(pi^2*d(2)^4)+0.5*8*rho_fa/(pi^2*d(2)^4);
k34 = k34+ 0.5*8*rho_fa/(pi^2*d(3)^4) + 0.5*8*rho_fa/(pi^2*d(3)^4)+ 2*8*rho_fa/(pi^2*d(3)^4);
k45 = k45+ 0.5*8*rho_fa/(pi^2*d(4)^4) + 1.3*8*rho_fa/(pi^2*d(4)^4);
k56 = k56+ 0.5*8*rho_fa/(pi^2*d(5)^4)+ 0.5*8*rho_fa/(pi^2*d(5)^4);
k67 = k67+ 1.3*8*rho_fa/(pi^2*d(6)^4);
%k78 = k78;
k89 = k89+ 0.5*8*rho_fa/(pi^2*d(8)^4)+0.5*8*rho_fa/(pi^2*d(8)^4)+ 1.3*8*rho_fa/(pi^2*d(8)^4)+ 0.5*8*rho_fa/(pi^2*d(8)^4);
k910 = k910+ 1.5*8*rho_fa/(pi^2*d(9)^4)+ 1.3*8*rho_fa/(pi^2*d(9)^4);
k310 = k310+ 0.5*8*rho_fa/(pi^2*d(10)^4) + 0.5*8*rho_fa/(pi^2*d(10)^4)+ 2*8*rho_fa/(pi^2*d(10)^4);
k311 = k311+ 0.5*8*rho_fa/(pi^2*d(11)^4)+0.5*8*rho_fa/(pi^2*d(11)^4)+ 2*8*rho_fa/(pi^2*d(11)^4);
k711 = k711+ 0.5*8*rho_fa/(pi^2*d(12)^4)+1.5*8*rho_fa/(pi^2*d(12)^4);
k312 = k312+ 2*8*rho_fa/(pi^2*d(13)^4)+ 0.5*8*rho_fa/(pi^2*d(13)^4)+ 2*8*rho_fa/(pi^2*d(13)^4);
k1213 = k1213 + 0.5*8*rho_fa/(pi^2*d(14)^4) + 0.5*8*rho_fa/(pi^2*d(14)^4);
k1314 = k1314 + 0.5*8*rho_fa/(pi^2*d(15)^4)+ 1.3*8*rho_fa/(pi^2*d(15)^4);
k1415 = k1415 +0.5*8*rho_fa/(pi^2*d(16)^4)+ 0.5*8*rho_fa/(pi^2*d(16)^4)+ 0.5*8*rho_fa/(pi^2*d(16)^4);
k1516 = k1516+ 1.3*8*rho_fa/(pi^2*d(17)^4);
%k1617 = k1617;
k1718 = k1718+0.5*8*rho_fa/(pi^2*d(19)^4)+ 0.5*8*rho_fa/(pi^2*d(19)^4)+ 1.3*8*rho_fa/(pi^2*d(19)^4);
k1819 = k1819+ 1.5*8*rho_fa/(pi^2*d(20)^4)+ 1.3*8*rho_fa/(pi^2*d(20)^4);
k1219 = k1219+ 0.5*8*rho_fa/(pi^2*d(21)^4) + 0.5*8*rho_fa/(pi^2*d(21)^4);
k1220 = k1220+ 0.5*8*rho_fa/(pi^2*d(22)^4) + 0.5*8*rho_fa/(pi^2*d(22)^4);
k1620 = k1620+ 1.5*8*rho_fa/(pi^2*d(23)^4)+0.5*8*rho_fa/(pi^2*d(23)^4);

%% Τοπολογία κόμβων
% Inputs 
NNODE = 20; %number of nodes
QINOUT = [0,0,0,-1.5*0.234/3600,-1.5*0.234/3600,-2*0.325/3600,0,-3*0.325/3600,-3*0.234/3600,-2*0.234/3600,-15*0.8/3600,0,-1.5*0.234/3600,-1.5*0.234/3600,-2*0.325/3600,0,-3*0.325/3600,-3*0.234/3600,-2*0.234/3600,-15*0.8/3600]; %in SI
QINOUT = 1.4*QINOUT;
QINOUT(1) = Qin;
p0 =[2505.4,2495.0,2489.1,2487.9,2486.8,2485.7,2484.6,2483.7,2482.7,2481.8,2480.5,2479.8,2478.5,2477.4,2476.3,2475.2,2474.2,2473.3,2472.4,2471.1]';

%% Τοπολογία στοιχείων δικτύου -connectivity
NELEM = 23;
INDXNODE = [1 2; 2 3; 3 4; 4 5; 5 6; 6 7; 7 8; 8 9; 9 10; 3 10; 3 11; 7 11; 3 12; 12 13; 13 14; 14 15; 15 16; 16 17; 17 18; 18 19; 12 19; 12 20; 16 20];
% Παράμετροι της γραμμής 
k_tel = [k12,k23,k34,k45,k56,k67,k78,k89,k910,k310,k311,k711,k312,k1213,k1314,k1415,k1516,k1617,k1718,k1819,k1219,k1220,k1620];
% Connectivity 
indxneib_element = zeros(NNODE,5);
indxneib_node = zeros(NNODE,5);
for i= 1:NELEM
[row,col] = find(INDXNODE == i);
for j=1:length(row)
  indxneib_element(i,j) = row(j);
  if (col(j) == 1)
  indxneib_node(i,j) = INDXNODE(row(j),2);
  else
  indxneib_node(i,j) = INDXNODE(row(j),1);
  end
end
end

%% Newton-Ralphson 
for i= 1:100
dF = dF_parametric(NNODE,indxneib_element,indxneib_node,p0,k_tel);
F = F_parametric(NNODE,QINOUT,indxneib_element,indxneib_node,p0,k_tel);
d_p0 = -F'/(dF');
d_p0 = d_p0';
p0 = p0 + d_p0;
end

p = p0;

end





