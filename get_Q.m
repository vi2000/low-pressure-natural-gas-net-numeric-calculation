function Q = get_Q(p0,k)
Q = zeros(1,23);
for i=1:9
Q(i) = abs(p0(i+1) - p0(i))/k(i);
end

Q(10) = abs(p0(10) - p0(3))/k(10);
Q(11) = abs(p0(11) - p0(3))/k(11);
Q(12) = abs(p0(7) - p0(11))/k(12);
Q(13) = abs(p0(12) - p0(3))/k(13);
for i=14:20
Q(i) = abs(p0(i-2) - p0(i-1))/k(i);
end
Q(21) = abs(p0(12) - p0(19))/k(21);
Q(22) = abs(p0(12) - p0(20))/k(22);
Q(23) = abs(p0(16) - p0(20))/k(23);
Q = sqrt(Q);
end 