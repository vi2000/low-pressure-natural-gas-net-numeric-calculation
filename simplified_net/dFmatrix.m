function dF = dFmatrix(h0A,h0B,h0C,kAB,kAC,kBC)
dFAA = 1;
dFBB = -0.5/sqrt(kAB)/(abs(h0A-h0B))^0.5 -0.5/sqrt(kBC)/(abs(h0B-h0C))^0.5;
dFCC = -0.5/sqrt(kAC)/(abs(h0A-h0C))^0.5 -0.5/sqrt(kBC)/(abs(h0B-h0C))^0.5;
dFAB = 0; 
dFAC = 0; 
dFBA = 0.5/sqrt(kAB)/(abs(h0A-h0B))^0.5; 
dFBC = 0.5/sqrt(kBC)/(abs(h0B-h0C))^0.5; 
dFCA = 0.5/sqrt(kAC)/(abs(h0A-h0C))^0.5; 
dFCB = 0.5/sqrt(kBC)/(abs(h0B-h0C))^0.5; 
dF = [dFAA dFAB dFAC; dFBA dFBB dFBC; dFCA dFCB dFCC];
end

