clc;
clear;
% Προσδιορισμός Q
% f_TX = 0.8;
% f_TR = 0.234;
% f_ME = 0.325;
% i = 1.4;
% Q_TX = 15*2;
% Q_TR = 2*(1.5+1.5+3+2);
% Q_ME = 5*2;
% Q_hr = (f_TX*Q_TX + f_TR*Q_TR + f_ME*Q_ME)*i;
% Q= Q_hr/3600;
Q =0.008609;
d_available = [21-2.6*2,26.9-2.6*2,33.7-2.6*2,42.4-2.6*2,48.3-2.6*2,60.3-2.9*2,76.1-2.9*2,88.9-3.2*2,114.3-3.6*2,139.7-4*2,168.3-4.5*2,219.1-5.9*2,273-6.3*2,323.9-7.1*2,355.6-7.1*2];
d_available = d_available/1000;
%d = d_available(6);
d = 0.0545;
u = Q*4/(pi*d^2);



lamda = get_lamda(Q,d);
k12 = k_linear(d,lamda,5);
k23 = k_linear(d,lamda,3.5);
k34 = k_linear(d,lamda,16);
k45 = k_linear(d,lamda,5);
k56 = k_linear(d,lamda,5);
k67 = k_linear(d,lamda,16);
k711 = k_linear(d,lamda,5);
k78 = k_linear(d,lamda,16);
k89 = k_linear(d,lamda,5);
k910 = k_linear(d,lamda,5);
k310 = k_linear(d,lamda,16);
k311 = k_linear(d,lamda,5);
k312 = k_linear(d,lamda,3);
k1213 = k_linear(d,lamda,16);
k1314 = k_linear(d,lamda,5);
k1415 = k_linear(d,lamda,5);
k1516 = k_linear(d,lamda,16);
k1617 = k_linear(d,lamda,16);
k1718 = k_linear(d,lamda,5);
k1819 = k_linear(d,lamda,5);
k1219 = k_linear(d,lamda,16);
k1220 = k_linear(d,lamda,5);
k1620 = k_linear(d,lamda,5);

% 
% rho_fa = 0.79;
% p01 = 2500 + rho_fa/2*u^2;
% p02 = p01 - k12*Q^2;
% p03 = p01 - (k12+k23)*Q^2;

% Τοπολογία κόμβων
% Inputs 
NNODE = 20; %number of nodes
% X = [16,16,16,0,0,0,16,2,32,32,16,16,0,0,0,16,32,32,32,16];
% Y = [0,5,5,5,10,15,15,15,10,5,10,5,5,10,15,15,15,10,5,10];
% Z = [0,0,3.5,3.5,3.5,3.5,3.5,3.5,3.5,3.5,3.5,6.5,6.5,6.5,6.5,6.5,6.5,6.5,6.5,6.5];
QINOUT = [0.008609+0.000003,0,0,-0.000098,-0.000098,-0.000181,0,-0.000271,-0.000195,-0.000130,-0.003333,0,-0.000098,-0.000098,-0.000181,0,-0.000271,-0.000195,-0.000130,-0.003333];
p0 =[2505.4,2495.0,2489.1,2487.9,2486.8,2485.7,2484.6,2483.7,2482.7,2481.8,2480.5,2479.8,2478.5,2477.4,2476.3,2475.2,2474.2,2473.3,2472.4,2471.1]';


% Τοπολογία στοιχείων δικτύου -connectivity
NELEM = 23;
INDXNODE = [1 2; 2 3; 3 4; 4 5; 5 6; 6 7; 7 8; 8 9; 9 10; 3 10; 3 11; 7 11; 3 12; 12 13; 13 14; 14 15; 15 16; 16 17; 17 18; 18 19; 12 19; 12 20; 16 20];
% Παράμετροι της γραμμής 
k_linear_tel = [k12,k23,k34,k45,k56,k67,k711,k78,k89,k910,k310,k311,k312,k1213,k1314,k1415,k1516,k1617,k1718,k1819,k1219,k1220,k1620];
% Connectivity 
indxneib_element = zeros(NNODE,5);
indxneib_node = zeros(NNODE,5);
for i= 1:NELEM
[row,col] = find(INDXNODE == i);
for j=1:length(row)
  indxneib_element(i,j) = row(j);
  if (col(j) == 1)
  indxneib_node(i,j) = INDXNODE(row(j),2);
  else
  indxneib_node(i,j) = INDXNODE(row(j),1);
  end
end
end

 for j = 1:100000
dF = dF_parametric(NNODE,indxneib_element,indxneib_node,p0,k_linear_tel);
F = F_parametric(NNODE,QINOUT,indxneib_element,indxneib_node,p0,k_linear_tel);
d_p0 = -F'/(dF');
d_p0 = d_p0';
p0 = p0 + d_p0;
 end