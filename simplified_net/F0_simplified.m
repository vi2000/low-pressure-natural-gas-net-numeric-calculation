function F0 = F0_simplified(p01,p02,p03,p04,p05,p06,p07,p08,p09,p010,p011,k12,k23,k34,k45,k56,k67,k711,k78,k89,k910,k310,k311)
%% node 1
F01 = 0;
%% node 2
if (p02 <= p01)
  sign1 = +1;
else
   sign1 = -1;
end
if (p02 <= p03)
   sign2 = +1;
else
    sign2 = -1;
end
 F02 = sign1*(abs((p01 -p02)/k12))^0.5 + sign2*(abs((p02- p03)/k23))^0.5;
 %% node 3
if (p03 <= p02)
   sign2 = +1;
else
   sign2 = -1;
end
if (p03 <= p04)
   sign3 = +1;
else
    sign3 = -1;
end
if (p03 <= p011)
   sign11 = +1;
else
   sign11 = -1;
end
if (p03 <= p010)
   sign10 = +1;
else
    sign10 = -1;
end
F03 =sign2*(abs((p03 - p02)/k23))^0.5 +sign3*(abs((p03 - p04)/k34))^0.5+sign11*(abs((p03 - p011)/k311))^0.5 +sign10*(abs((p03 - p010)/k310))^0.5;

%% node 4 
if (p04 <= p03)
  sign3 = +1;
else
   sign3 = -1;
end
if (p04 <= p05)
   sign4 = +1;
else
    sign4 = -1;
end
F04 = -0.000098 + sign3*(abs((p03 -p04)/k34))^0.5 + sign4*(abs((p04- p05)/k45))^0.5;
%% node 5 
if (p05 <= p04)
  sign4 = +1;
else
   sign4 = -1;
end
if (p05 <= p06)
   sign5 = +1;
else
    sign5 = -1;
end
F05 = -0.000098 + sign4*(abs((p05 -p04)/k45))^0.5 + sign5*(abs((p06- p05)/k56))^0.5;
%% node 6
if (p06 <= p05)
  sign5 = +1;
else
   sign5 = -1;
end
if (p06 <= p07)
   sign6 = +1;
else
    sign6 = -1;
end
F06 = -0.000181 + sign5*(abs((p05 -p06)/k56))^0.5 + sign6*(abs((p06- p07)/k67))^0.5;
%% node 7
if (p07 <= p06)
  sign6 = +1;
else
   sign6 = -1;
end
if (p07 <= p011)
   sign12 = +1;
else
    sign12 = -1;
end
if (p07 <= p08)
  sign7 = +1;
else
   sign7 = -1;
end
F07 =sign6*(abs((p07 - p06)/k67))^0.5 +sign12*(abs((p07 -p011)/k711))^0.5+sign7*(abs((p07 - p08)/k78))^0.5;
%% node 8
if (p08 <= p07)
  sign7 = +1;
else
   sign7 = -1;
end
if (p08 <= p09)
   sign8 = +1;
else
    sign8 = -1;
end
F08 = -0.000271 + sign7*(abs((p08 -p07)/k78))^0.5 + sign8*(abs((p08- p09)/k89))^0.5;
%% node 9 
if (p09 <= p08)
  sign8 = +1;
else
   sign8 = -1;
end
if (p09 <= p010)
   sign9 = +1;
else
    sign9 = -1;
end
F09 = -0.000195 + sign8*(abs((p09 -p08)/k89))^0.5 + sign9*(abs((p09- p010)/k910))^0.5;
%% node 10 
if (p010 <= p09)
  sign9 = +1;
else
   sign9 = -1;
end
if (p010 <= p03)
   sign10 = +1;
else
    sign10 = -1;
end
F010 = -0.000130 + sign9*(abs((p09 -p010)/k910))^0.5 + sign10*(abs((p03- p010)/k310))^0.5;
%% node 11 
if (p011 <= p03)
  sign11 = +1;
else
   sign11 = -1;
end
if (p011 <= p07)
   sign12 = +1;
else
    sign12 = -1;
end
F011 = -0.003333 + sign11*(abs((p011 -p03)/k311))^0.5 + sign12*(abs((p011- p07)/k711))^0.5;
%%
F0 = [F01; F02;F03; F04; F05; F06; F07; F08; F09; F010;F011];
end