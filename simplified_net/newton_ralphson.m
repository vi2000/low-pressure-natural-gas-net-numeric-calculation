%% Input Parameters 
k1 = 50000; %s^2/m^5
z = 10;
C1 = 0.00417; %m^3/s
C2 = 0.00833; %m^3/s
kAB = 20000; %s^2/m^5
kAC = 15000;
kBC = 10000;
Q = C1+C2;
%% initial conditions 
h0A=2.18750; 
h0B = 1.90000; 
h0C =1.80000;
h0 = [h0A; h0B; h0C];
dF = dFmatrix(h0A,h0B,h0C,kAB,kAC,kBC);
F = Fvector(h0A,h0B,h0C,kAB,kAC,kBC,C1,C2);
d_h0 = -F'/(dF');indxneib_element
d_h0 = d_h0';


while (abs(d_h0(2)) > 1e-10 && abs(d_h0(3)) > 1e-10)
h0 = h0 + d_h0;
h0A = h0(1); 
h0B = h0(2); 
h0C = h0(3);
dF = dFmatrix(h0A,h0B,h0C,kAB,kAC,kBC);
F = Fvector(h0A,h0B,h0C,kAB,kAC,kBC,C1,C2);
d_h0 = -F'/(dF');
d_h0 = d_h0';
end
