%% Input Parameters 
Q =0.008609/2;
d = 0.0545;
lamda = get_lamda(Q,d);
k12 = k_linear(d,lamda,5);
k23 = k_linear(d,lamda,3.5);
k34 = k_linear(d,lamda,16);
k45 = k_linear(d,lamda,5);
k56 = k_linear(d,lamda,5);
k67 = k_linear(d,lamda,16);
k711 = k_linear(d,lamda,5);
k78 = k_linear(d,lamda,16);
k89 = k_linear(d,lamda,5);
k910 = k_linear(d,lamda,5);
k310 = k_linear(d,lamda,16);
k311 = k_linear(d,lamda,5);
%% Bernoulli
% z = 3.5;
% rho_fa = 0.79;
% rho_a = 1.225;
% p1 = 2500;
% g = 9.81;
% %p3 = p1 -(rho_fa -rho_a)*g*z - (k12+k23)*Q
% u = Q*4/(pi*d^2);
% p01 = 2505.4 + rho_fa/2*u^2;
% p02 = p01 - k12*Q;
% p03 = p01 - (k12+k23)*Q;
%% initial conditions 
p01 = 2505.4;
p02 = 2490.0;
p03 = 2485.0;
p04 = 2480;
p05 = 2475;
p06 = 2470;
p07 =2465;
p08 =2460;
p09 =2455;
p010 =2450;
p011 =2445;

p0 = [p01;p02;p03;p04;p05;p06;p07;p08;p09;p010;p011];
dF = dF_simplified(p01,p02,p03,p04,p05,p06,p07,p08,p09,p010,p011,k12,k23,k34,k45,k56,k67,k711,k78,k89,k910,k310,k311);
F0 = F0_simplified(p01,p02,p03,p04,p05,p06,p07,p08,p09,p010,p011,k12,k23,k34,k45,k56,k67,k711,k78,k89,k910,k310,k311);
d_p0 = -F0'/(dF');
d_p0 = d_p0';
% d_p0 = dF\F0;
% p0 = p0 + d_p0;


p0 = p0 + d_p0;
p01 = p0(1);
p02 = p0(2);
p03 = p0(3);
p04 = p0(4);
p05 = p0(5);
p06 = p0(6);
p07 = p0(7);
p08 = p0(8);
p09 = p0(9);
p010 = p0(8);
p011= p0(9);
p0 = [p01;p02;p03;p04;p05;p06;p07;p08;p09;p010;p011];
dF = dF_simplified(p01,p02,p03,p04,p05,p06,p07,p08,p09,p010,p011,k12,k23,k34,k45,k56,k67,k711,k78,k89,k910,k310,k311);
F0 = F0_simplified(p01,p02,p03,p04,p05,p06,p07,p08,p09,p010,p011,k12,k23,k34,k45,k56,k67,k711,k78,k89,k910,k310,k311);
d_p0 = -F0'/(dF');
d_p0 = d_p0';
