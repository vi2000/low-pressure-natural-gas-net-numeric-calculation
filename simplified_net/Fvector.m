function F = Fvector(h0A,h0B,h0C,kAB,kAC,kBC,C1,C2)

FA = 0;
if (h0B <= h0C)
  sign3 = +1;
else
   sign3 = -1;
end
if (h0B <= h0A)
   sign2 = +1;
else
    sign2 = -1;
end
FB = -C1 + sign2*(abs((h0B -h0A)/kAB))^0.5 +sign3*(abs((h0B -h0C)/kBC))^0.5;
if (h0C <= h0B)
   sign3 = +1;
else
   sign3 = -1;
end
if (h0C <= h0A)
   sign4 = +1;
else
    sign4 = -1;
end
FC = -C2 + sign3*(abs((h0C -h0B)/kBC))^0.5 +sign4*(abs((h0A -h0C)/kAC))^0.5;
%% Orismos dF
F = [FA;FB;FC];
end

