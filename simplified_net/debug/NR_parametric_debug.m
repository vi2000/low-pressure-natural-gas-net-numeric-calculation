%% Input Parameters 
C1 = 0.00417; %m^3/s
C2 = 0.00833; %m^3/s
kAB = 20000; %s^2/m^5
kAC = 15000;
kBC = 10000;
Q = C1+C2;
%% initial conditions 
h0A=2.18750; 
h0B = 1.90000; 
h0C =1.80000;

% Τοπολογία κόμβων
% Inputs 
NNODE = 3; %number of nodes
QINOUT = [Q,-C1,-C2]; %in SI
h0 = [h0A,h0B,h0C]'; % in Pa
% Τοπολογία στοιχείων δικτύου -connectivity
NELEM = 3;
INDXNODE = [1 2; 2 3; 3 1];
% Παράμετροι της γραμμής 
k_linear_tel = [kAB,kBC,kAC];
% Connectivity 
indxneib_element = zeros(NNODE,5);
indxneib_node = zeros(NNODE,5);
for i= 1:NELEM
[row,col] = find(INDXNODE == i);
for j=1:length(row)
  indxneib_element(i,j) = row(j);
  if (col(j) == 1)
  indxneib_node(i,j) = INDXNODE(row(j),2);
  else
  indxneib_node(i,j) = INDXNODE(row(j),1);
  end
end
end

for i= 1:100000
dF = dF_parametric(NNODE,indxneib_element,indxneib_node,h0,k_linear_tel);
F = F_parametric(NNODE,QINOUT,indxneib_element,indxneib_node,h0,k_linear_tel);
d_h0 = -dF\F;
h0 = h0 + d_h0;
end






