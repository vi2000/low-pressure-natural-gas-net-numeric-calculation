function k = k_linear(d,lamda,l)
%for h
g = 9.81;
k = lamda*l*8/(pi^2*g*d^5);
end