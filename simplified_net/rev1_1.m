% Calculate Q 
f_TX = 0.8;
f_TR = 0.234;
f_ME = 0.325;
i = 1.4;
Q_TX = 15*2;
Q_TR = 2*(1.5+15+3+2);
Q_ME = 5*2;
Q_hr = (f_TX*Q_TX + f_TR*Q_TR + f_ME*Q_ME)*i;
Q= Q_hr/3600;
% γραμμικές απώλειες
d = sqrt(Q/pi)*1000;
d_typ = 70.3/1000;
lamda = get_lamda(Q,d_typ);

l = [5,3.5,16,5,5,16,16,5,5,16,5,5,3,16,5,5,16,16,5,5,16,5,5];
k = k_linear(d_typ,lamda,l);
function lamda = get_lamda(Q,d)
u = 4*Q/(pi*d^2);
Re = u*d/(13.57*10^(-6));
epsilon = 0.05/1000;
syms lamda
lamda = double(vpasolve(1/sqrt(lamda) == 1.14 - 2*log10(epsilon/d + 21.15/Re^0.9), lamda));
end

function k = k_linear(d,lamda,l)
g = 9.81;
k = lamda*l*8/(pi^2*g*d^5);
end


