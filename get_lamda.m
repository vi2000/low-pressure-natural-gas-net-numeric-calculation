function lamda = get_lamda(Q,d)
lamda = zeros(1,23);
Re = zeros(1,23);
u = zeros(1,23);
for i = 1:23
u(i) = 4*Q(i)/(pi*d(i)^2);
Re(i) = u(i)*d(i)/(13.57*10^(-6));
epsilon = 0.05/1000;
syms l
%lamda = double(vpasolve(1/sqrt(lamda) == 1.14 - 2*log10(epsilon/d ), lamda));
l = double(vpasolve(1/sqrt(l) == 1.14 - 2*log10(epsilon/d(i) + 21.15/Re(i)^0.9), l));
lamda(i) = l;
end
end