function k = k_linear(d,lamda,l)
rho_fa = 0.79;
k = lamda*l*8*rho_fa/(pi^2*d^5);
end